package apiTest;

public class constants {

    public static final String DataValidationInExcel = "dataValidation" ;
    public static final String DirPath = "src/test/java/TestData";
    public static final String actualPath = "src/test/java/apiTest/TestData/Words.xlsx";
    public static final String expectedExcelPath = "src/test/java/apiTest/TestData/Words1.xlsx";


    public static final String DataValidationInPDF = "dataValidation" ;
    public static final String actualPathForPdf = "src/test/java/apiTest/TestData/pdf-test.pdf";
    public static final String expectedPDFPath = "src/test/java/apiTest/TestData/pdf-test1.pdf";

}
